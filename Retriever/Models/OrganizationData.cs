﻿namespace Moa.DataServices.SmevEgrul.Retriever.Models
{
    internal class OrganizationData
    {
        public OrganizationData(string inn)
        {
            Inn = inn;
        }

        public string Inn { get; internal set; }
    }
}