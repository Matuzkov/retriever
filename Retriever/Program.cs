﻿using MassTransit;
using Moa.DataServices.SmevEgrul.Retriever.Models;
using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace Moa.DataServices.SmevEgrul.Retriever
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            EndpointConvention.Map<OrganizationData>(
                new Uri("rabbitmq://http://127.0.0.1:15672/serg/serg_processing");

            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host("127.0.0.1", 15672, "/", h =>
                {
                    h.Username("user");
                    h.Password("VV1cBuk4ew");

                    h.UseSsl(s =>
                    {
                        s.Protocol = SslProtocols.Tls;
                    }); ;
                });

                cfg.ReceiveEndpoint("serg", e =>
                {
                    e.Consumer<SergEventConsumer>();
                });
            });

            var source = new CancellationTokenSource(TimeSpan.FromSeconds(20));

            await busControl.StartAsync(source.Token);
            try
            {
                Console.WriteLine("Press enter to exit");

                await Task.Run(() => Console.ReadLine());
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
    }

    internal class SergEventConsumer : IConsumer<OrganizationData>
    {
        public async Task Consume(ConsumeContext<OrganizationData> context)
        {
            Console.WriteLine("Serg Submitted: {0}", context.Message.Inn);
        }
    }
}